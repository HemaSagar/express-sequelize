const db = require("../models");

const Tutorial = db.tutorials;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
  if (!req.body.title) {
    res.status(400).send({
      message: "content can't be empty",
    });
    return;
  }

  const tutorial = {
    title: req.body.title,
    description: req.body.description,
    published: req.body.published ? req.body.published : false,
  };

  Tutorial.create(tutorial)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Error occurred while creating tutorial",
      });
    });
};
exports.findAll = (req, res) => {

  const title = req.query.title;
  const condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Tutorial.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "error occured while retrieving",
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Tutorial.findByPk(id)
    .then((data) => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Can't find tutorial with id: ${id}`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Error occured while retriving",
      });
    });
};
exports.update = (req, res) => {
  const id = req.params.id;
  const data = req.body;

  Tutorial.update(data, {
    where: { id: id },
  })
    .then((result) => {
      if (result == 1) {
        res.send({
          message: `Tutorial with id: ${id} updated succesfully`,
        });
      } else {
        res.send({
          message: `Can't update, no tutorial found with id: ${id}`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error updating tutorial with id: ${id}`,
      });
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Tutorial.destroy({
    where: { id: id },
  })
    .then((result) => {
      if (result == 1) {
        res.send({
          message: `Tutorial with id: ${id} deleted succesfully`,
        });
      } else {
        res.send({
          message: `Tutorial with id: ${id} not deleted, maybe no tutorial with that id`,
        });
      }
    })
    .catch((err) => {
      res.send({
        message: `Can't delete tutorial with id:${id}`,
      });
    });
};

exports.deleteAll = (req, res) => {
  Tutorial.destroy({
    where: {},
    truncate: false,
  })
    .then((number) => {
      res.send({
        message: `${number} tutorials deleted`,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Error occurred while deleting all tutorials",
      });
    });
};

exports.findAllPublished = (req, res) => {
  Tutorial.findAll({
    where: { published: true },
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error occurred while retrieving tutorials",
      });
    });
};

