const express = require("express");
const cors = require("cors");
const db = require("./app/models");
const routes = require("./app/routes/tutorial.routes.js");

const port = process.env.port || 4000;

const app = express();

let corsOptions = {
  origin: "*",
};

app.use(cors(corsOptions));

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.json({ message: "Welcome to server" });
});

routes(app);

db.sequelize
  .sync()
  .then(() => {
    console.log("Synced db.");    
  })
  .catch((err) => {
    console.log("Failed to sync db: " + err.message);
  });

app.listen(port, () => {
  console.log(`server listening to port ${port}`);
});
